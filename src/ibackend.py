# Using modified strategy pattern

import abc


class BackendRegistry(object):
    '''
    Registry class for registering the backends for asset management
    '''

    __registry = {}  # registered backends
    __backend = None  # active (current  backend)

    @classmethod
    def register(cls, backend):
        '''
        registers a backend for asset management
        @param: backend is instantance of Backend class
        '''
        assert (isinstance(backend, Backend))
        if not cls.__registry:
            cls.__backend = backend
        cls.__registry[backend.name] = backend

    @classmethod
    def set(cls, name):
        '''
        sets the active (current) backend from registered backends
        '''
        try:
            cls.__backend = cls.__registry[name]
        except KeyError:
            # log the error
            print('No backend registered for %s' % name)

    @classmethod
    def names(cls):
        '''
        returns the names of registered backends
        '''
        return cls.__registry.keys()

    @classmethod
    def get(cls):
        '''
        returns the active (current) backend
        '''
        return cls.__backend


class Backend(object):
    '''
    backend class to store and serve all the available backend classes
    '''

    def __init__(self, name):
        self._classes = {}  # all the available classes
        self._name = name  # name of the backend (eg. tactic, shotgun)

    @property
    def name(self):
        '''
        returns the name of the backend
        '''
        return self._name

    def _getClass(self, name):
        '''
        return the specified class from the backend
        '''
        return self._classes[name]

    def addClass(self, concreteClass):
        '''
        add new class to the backend
        '''
        if not concreteClass.__entity_type__:
            raise ValueError('__entity_type__ not found')
        self._classes[concreteClass.__entity_type__] = concreteClass

    def __dir__(self):
        _dir = self.__dict__.keys()
        _dir.extend(self.__class__.__dict__.keys())
        _dir.extend(self._classes.keys())
        return _dir

    def __getattr__(self, name):
        return self._getClass(name)

    def __str__(self):
        return self._name


class IEntity(object):
    '''
    Base class for all the entities in the asset management
    '''
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def setProject(self, project):
        pass

    @abc.abstractmethod
    def getProject(self):
        pass

    @abc.abstractmethod
    def browseLocation(self):
        pass

    @abc.abstractproperty
    def __entity_type__(cls):
        '''
        returns the concrete entity type, the name to be used in GUI
        '''
        pass

    @abc.abstractmethod
    # class method
    def all(cls, order_bys=None):
        '''
        returns all the available entity objects (e.g all projects, productionElements)
        '''
        pass

    @abc.abstractmethod
    # class method
    def get(cls, filters=None, order_bys=None):
        '''
        returns a single entity object (e.g. a project, asset, productionElement)
        '''
        pass

    @abc.abstractmethod
    def filter(cls, filters=None, order_bys=None):
        '''
        returns multiple entities according to the filters and orders provided
        '''
        pass

    @abc.abstractmethod
    # class method
    def create(cls, obj):
        '''
        create a single entity object (e.g. a project, asset, productionElement) in the
        database
        '''
        pass

    @abc.abstractmethod
    def save(self):
        '''
        save this objects stat to database
        '''
        pass

    @abc.abstractmethod
    def remove(self):
        '''
        remove this entity from database
        '''
        pass

    @abc.abstractproperty
    def path(self):
        '''
        file system path of the entity
        '''
        pass

    @abc.abstractmethod
    def update(self, entity):
        ''' Update the fields of the entity from the given entity
        '''
        pass

    @abc.abstractproperty
    def snapshots(self):
        pass

    @abc.abstractmethod
    def get_snapshots(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def createMultiple(cls, sobjs):
        pass

    @abc.abstractmethod
    def saveMultiple(cls, sobjs):
        pass

    @abc.abstractproperty
    def identity(self):
        pass

    @abc.abstractmethod
    def exists(self):
        pass

    @abc.abstractproperty
    def isDirty(self):
        pass

    @abc.abstractproperty
    def thumbnail(self):
        '''
        returns the thumbnail of the project
        '''
        pass

    @abc.abstractmethod
    def get_thumbnail(*args, **kwargs):
        # get thumb with any args
        pass

    @abc.abstractproperty
    def key(self):
        pass


class IProject(IEntity):

    __entity_type__ = 'Project'

    @abc.abstractproperty
    def __templates__(self):
        pass

    @abc.abstractproperty
    def __categories__(self):
        pass

    @abc.abstractproperty
    def active(self):
        '''
        returns true if the project is Active, false otherwise
        '''
        pass

    @abc.abstractproperty
    def title(self):
        pass

    @abc.abstractproperty
    def category(self):
        '''
        returns typ of the project (internal, external, local)
        '''
        pass

    @abc.abstractproperty
    def template(self):
        '''
        returns template name of the project (episodic, commercial)
        '''
        pass


class ICategory(IEntity):
    __entity_type__ = 'Category'


class IAsset(IEntity):
    __entity_type__ = 'Asset'

    @abc.abstractproperty
    def name(self):
        pass

    @abc.abstractproperty
    def category(self):
        '''
        returns the category of the asset
        '''
        pass

    @abc.abstractproperty
    def productionElement(self):
        '''
        returns the productionElement this asset is published or used in
        '''
        pass

    @abc.abstractmethod
    def get_productionElement(*args, **kwargs):
        '''
        returns productionElement(s) with args
        '''
        pass


class ISnapshot(IEntity):
    '''
    deals with the files saved in IEntity objects
    '''
    __entity_type__ = 'Snapshot'

    @abc.abstractproperty
    def files(self):
        '''
        returns all the files within snapshot
        '''
        pass

    @abc.abstractproperty
    def parent_code(self):
        '''
        return the parent of the snapshot
        '''
        pass

    @abc.abstractmethod
    def addFiles(self):
        '''
        add files to an existing snapshot
        '''
        pass


class IFile(IEntity):
    '''
    Representation of a file in a file system
    '''
    __entity_type__ = 'File'


class IProductionElement(IEntity):
    # TODO: use chain of resposibility pattern
    name = abc.abstractproperty()
    sort_order = abc.abstractproperty()
    description = abc.abstractproperty()

    @abc.abstractproperty
    def children():
        pass

    @abc.abstractmethod
    def parent():
        pass

    @abc.abstractmethod
    def get_children(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def get_parent(self, *args, **kwargs):
        pass

    @abc.abstractproperty
    def preceding(self):
        pass

    @abc.abstractmethod
    def get_preceding(self, *args, **kwargs):
        pass

    @abc.abstractproperty
    def succeeding(self):
        pass

    @abc.abstractmethod
    def get_succeeding(self, *args, **kwargs):
        pass

    @abc.abstractproperty
    def overlapping(self):
        pass

    @abc.abstractmethod
    def get_overlapping(self, *args, **kwargs):
        pass


class IEpisode(IProductionElement):
    __entity_type__ = 'Episode'

    @abc.abstractproperty
    def sequences(self):
        pass

    @abc.abstractmethod
    def get_sequences(self, *args, **kwargs):
        pass

    @abc.abstractproperty
    def shots(self):
        pass

    @abc.abstractmethod
    def get_shots(self, *args, **kwargs):
        pass


class ISequence(IProductionElement):
    __entity_type__ = 'Sequence'
    episode = abc.abstractproperty()

    @abc.abstractproperty
    def shots(self):
        pass

    @abc.abstractmethod
    def get_shots(self, *args, **kwargs):
        pass


class IShot(IProductionElement):
    __entity_type__ = 'Shot'
    # TODO: use aggregation for animation, lighting and compositing

    sequence = abc.abstractproperty()

    start_frame = abc.abstractproperty()
    end_frame = abc.abstractproperty()
    frame_in = abc.abstractproperty()
    frame_out = abc.abstractproperty()


class IEpisodeAsset(IEntity):
    __entity_type__ = 'EpisodeAsset'

    @abc.abstractproperty
    def name(self):
        pass


class ISequenceAsset(IEntity):
    __entity_type__ = 'SequenceAsset'
    pass


class IShotAsset(IEntity):
    __entity_type__ = 'ShotAsset'
    pass


class IUser(IEntity):
    __entity_type__ = 'User'
    pass


class IGroup(IEntity):
    __entity_type__ = 'Group'

    @abc.abstractmethod
    def addUsers(self, users):
        pass

    @abc.abstractmethod
    def getUsers(self):
        pass


class IGroupUser(IEntity):
    __entity_type__ = 'GroupUser'
    pass


class IPipeline(IEntity):
    __entity_type__ = 'Pipeline'
    pass
